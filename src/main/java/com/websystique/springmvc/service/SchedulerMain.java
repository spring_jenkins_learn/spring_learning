package com.websystique.springmvc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.Calendar;

/**
 * Created by mickey on 20/04/17.
 */
@EnableScheduling
@Configuration
public class SchedulerMain {

    @Autowired
    ServiceCron serviceCron;

    @Scheduled(fixedDelay = 2000)
    public void scheduled1(){
        Calendar calendario = Calendar.getInstance();
        int hora = calendario.get(Calendar.HOUR_OF_DAY);
        int minutos = calendario.get(Calendar.MINUTE);
        int segundos = calendario.get(Calendar.SECOND);
       // System.out.println("[scheduled] ==> ["+hora + ":" + minutos + ":" + segundos+"]");
    }

    @Scheduled(cron = "*/25 * * * * *")
    public void databaseChecker() {
        serviceCron.check();
        System.out.println("[databaseChecker] => changes not found!");
    }

}
