package com.websystique.springmvc.controller.exceptions.exception;

import com.websystique.springmvc.controller.exceptions.enums.CodeExceptionEnum;

/**
 * Created by mickey on 17/04/17.
 */
public class CustomExceptionROSA extends Exception {

    CodeExceptionEnum code;

    public CustomExceptionROSA(CodeExceptionEnum CODE) {
        super(CODE.toString());
        code = CODE;
    }
    public CustomExceptionROSA(Exception msg,CodeExceptionEnum CODE) {
        super(msg);
        code = CODE;
    }

    public CustomExceptionROSA() {
    }

    public CustomExceptionROSA(String message) {
        super(message);
    }

    public CustomExceptionROSA(String message, Throwable cause) {
        super(message, cause);
    }

    public CustomExceptionROSA(Throwable cause) {
        super(cause);
    }

    public CustomExceptionROSA(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public CodeExceptionEnum getCode() {
        return code;
    }

    public void setCode(CodeExceptionEnum code) {
        this.code = code;
    }
}
