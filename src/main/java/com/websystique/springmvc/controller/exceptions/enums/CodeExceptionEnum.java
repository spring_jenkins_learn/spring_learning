package com.websystique.springmvc.controller.exceptions.enums;

/**
 * Created by mickey on 17/04/17.
 */
public enum CodeExceptionEnum {

    ERR_01("ERR_01","ERROR NUMBER FORMAT EXCEPTION"),
    ERR_02("ERR_02","ERROR NULL POINTER EXCEPTION EXCEPTION");

    private String msg;
    private String code;

    CodeExceptionEnum(String code, String msg) {
        this.msg = msg;
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public String getCode() {
        return code;
    }

    @Override
    public String toString() {
        return "CodeExceptionEnum{" +
                "msg='" + msg + '\'' +
                ", code='" + code + '\'' +
                '}';
    }
}
