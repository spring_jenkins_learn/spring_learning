package com.websystique.springmvc.controller.exceptions;

import com.websystique.springmvc.controller.exceptions.exception.CustomExceptionROSA;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by migsanch on 17/04/2017.
 */
@ControllerAdvice
class ExceptionController {

    public static final String DEFAULT_ERROR_VIEW = "error";

   @ExceptionHandler(value = CustomExceptionROSA.class)
    public ModelAndView defaultErrorHandler(HttpServletRequest req, CustomExceptionROSA e) throws Exception {
        ModelAndView mav = new ModelAndView();
        e.printStackTrace();
        mav.addObject("exception", e.getCode().getCode());
        mav.addObject("url", req.getRequestURL());
        mav.setViewName(DEFAULT_ERROR_VIEW);
        return mav;
    }

    @ExceptionHandler(value = Exception.class)
    public ModelAndView defaultErrorHandler(HttpServletRequest req, Exception e) throws Exception {
        ModelAndView mav = new ModelAndView();
        mav.addObject("exception", e);
        mav.addObject("url", req.getRequestURL());
        mav.setViewName(DEFAULT_ERROR_VIEW);
        return mav;
    }


}
