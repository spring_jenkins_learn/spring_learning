package com.websystique.springmvc.configuration.security;

import com.websystique.springmvc.configuration.security.roles.EnumRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import javax.sql.DataSource;

/**
 * Created by mickey on 27/04/17.
 */
@Configuration
@EnableWebSecurity
public class SecurityConfig  extends WebSecurityConfigurerAdapter{

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication().withUser("mike").password("pokemon").roles(
                EnumRoles.USER.toString(),EnumRoles.ADMIN.toString(),EnumRoles.DBA.toString());
        auth.inMemoryAuthentication().withUser("admin").password("123456").roles(EnumRoles.ADMIN.toString());
        auth.inMemoryAuthentication().withUser("dba").password("123456").roles(EnumRoles.DBA.toString());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/secured/admin/**").access("hasRole('ROLE_ADMIN')")
                .antMatchers("/secured/dba/**").access("hasRole('ROLE_ADMIN') or hasRole('ROLE_DBA')")
                .and().formLogin().loginPage("/secured/login").defaultSuccessUrl("/secured/")
                //.usernameParameter("user").passwordParameter("pwd")
                .and().logout().logoutUrl("/secured/logout")
                .and().exceptionHandling().accessDeniedPage("/secured/");
    }

}
