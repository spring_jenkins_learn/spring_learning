package com.websystique.springmvc.configuration.security.roles;

/**
 * Created by mickey on 27/04/17.
 */
public enum EnumRoles {

    USER("USER"),
    ADMIN("ADMIN"),
    DBA("DBA");

    private String rol;

    EnumRoles(String rol) {
        this.rol = rol;

    }

    @Override
    public String toString() {
        return rol;
    }
}
