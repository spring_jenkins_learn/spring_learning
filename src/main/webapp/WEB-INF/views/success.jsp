<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
  <title>Student Enrollment Detail Confirmation</title>
  <link href="<c:url value='/static/css/custom.css' />" rel="stylesheet"></link>
  <link href="<c:url value='/static/css/bootstrap.css' />" rel="stylesheet"></link>
</head>
<body>
  <div class="container">
    <div class="row">
      <div class="page-header">
        <h1>Enrollment Form <small>Spring mvc example</small></h1>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6 col-md-offset-3">

        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">Confirmation message :</h3>
          </div>
          <div class="panel-body">
            <p>
              ${success}
            </p>
            <p>
              We have also sent you a confirmation mail to your email address : ${student.email}.
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</body>
</html>