<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Student Enrollment Form</title>
	<link href="<c:url value='/static/css/bootstrap.css' />" rel="stylesheet"></link>
	<link href="<c:url value='/static/css/custom.css' />" rel="stylesheet"></link>
</head>

<body>
 <div class="container">
    <div class="row">
        <div class="page-header">
            <h1>ERROR <small>http</small></h1>
        </div>
    <div>
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
        <div class="panel panel-danger">
          <div class="panel-heading">404</div>
          <div class="panel-body">
                NOT FOUND
          </div>
        </div>
        </div>
    </div>
 </div>
</body>
</html>
