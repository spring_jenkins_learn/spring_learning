<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
  <title>Student Enrollment Detail Confirmation</title>
  <link href="<c:url value='/static/css/custom.css' />" rel="stylesheet"></link>
  <link href="<c:url value='/static/css/bootstrap.css' />" rel="stylesheet"></link>
</head>
<body>
  <div class="container">
    <div class="row">
      <div class="page-header">
        <h1>Enrollment Form <small>Spring mvc example</small></h1>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6 col-md-offset-3">

        <div class="panel panel-primary">
          <div class="panel-heading">
            <h3 class="panel-title"> ${title} ${message}</h3>
          </div>
          <div class="panel-body">
            <c:if test="${pageContext.request.userPrincipal.name != null}">
            <p class="text-center">
              Welcome : ${pageContext.request.userPrincipal.name}


<sec:authentication property="principal.authorities" var="authorities" />
<c:forEach items="${authorities}" var="authority" varStatus="vs">
<p>${authority.authority}</p>
</c:forEach>


<sec:authorize access="hasRole('ADMIN')">

This content will only be visible to users who have
the "supervisor" authority in their list of <tt>GrantedAuthority</tt>s.

</sec:authorize>


            </p>
            <p class="text-center">
              <a class="btn btn-danger" href="javascript:formSubmit()" role="button">Logout</a>
            </p>
          </c:if>
        </div>
      </div>
    </div>
  </div>
</div>

  <!-- csrt for log out-->
  <form action="/secured/logout" method="POST" id="logoutForm">
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
  </form>

  <script>
  function formSubmit() {
    document.getElementById("logoutForm").submit();
  }
  </script>
</body>
</html>