<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
  <title>Login</title>
  <link href="<c:url value='/static/css/custom.css' />" rel="stylesheet"></link>
  <link href="<c:url value='/static/css/bootstrap.css' />" rel="stylesheet"></link>
</head>
<body>
  <div class="container">
   <div class="row">
     <div class="page-header">
      <h1>Spring  <small>mvc secured</small></h1>
    </div>
  </div>
  <div class="row">
    <div class="col-md-6 col-md-offset-3">
      <div class="panel panel-primary">
        <div class="panel-heading">Login form</div>
        <div class="panel-body">
          <form name='loginForm' login-processing-url="/j_spring_security_check" method='POST'>
            <div class="form-group">
              <label for="exampleInputEmail1">User name</label>
              <input class="form-control" type='text' name='username'>
            </div>
            <div class="form-group">
              <label for="exampleInputPassword1">Password</label>
              <input type='password' class="form-control" name='password'>
            </div>

            <!-- hidden value for spring security -->
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
            <!-- input -->
            <button type="submit" class="btn btn-primary">login</button>
          </form>
                <!-- error -->
                <c:if test="${not empty error}">
                <div class="error" style="color:#A90115">${error}</div>
              </c:if>
              <c:if test="${not empty msg}">
              <div class="msg" style="color:#5BC501">${msg}</div>
            </c:if>
        </div>
      </div>
</div>
</div>
</div>
</body>
</html>