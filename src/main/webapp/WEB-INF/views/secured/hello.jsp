<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
  <title>Student Enrollment Detail Confirmation</title>
  <link href="<c:url value='/static/css/custom.css' />" rel="stylesheet"></link>
  <link href="<c:url value='/static/css/bootstrap.css' />" rel="stylesheet"></link>
</head>
<body>
  <div class="container">
    <div class="row">
      <div class="page-header">
        <h1>Enrollment Form <small>Spring mvc example</small></h1>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6 col-md-offset-3">

        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">${tittle} ${message}</h3>
          </div>
          <div class="panel-body">
            <c:if test="${pageContext.request.userPrincipal.name != null}">
            <p class="text-center">
             Welcome : ${pageContext.request.userPrincipal.name}
           </p>
           <p class="text-center">
             <a class="btn btn-danger" href="javascript:formSubmit()" role="button">Logout</a>
           </p>
         </c:if>
       </div>
     </div>
   </div>
 </div>
</div>
  <!-- variable for logout url ->
      <c:url value="/secured/logout" var="logoutUrl" />
      <!-- csrt for log out-->
      <form action="${logoutUrl}" method="post" id="logoutForm">
        <input type="hidden"
        name="${_csrf.parameterName}"
        value="${_csrf.token}" />
      </form>

      <script>
      function formSubmit() {
        document.getElementById("logoutForm").submit();
      }
      </script>
    </body>
    </html>
